package com.anand.firstapp.repositories;

import com.anand.firstapp.domain.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {
}
